#include <SPI.h>
#include <MFRC522.h>
#include <RTClib.h>
#include <WiFi.h>
#include "time.h"
#include <Wire.h>

RTC_DS3231 rtc;

//RTClib RTC;
#define RST_PIN 9
MFRC522 mfrc522(RST_PIN);


//For RTC
char t[32];
char daysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};



// For WIFI
const char* ssid     = "ARUN";         // The SSID (name) of the Wi-Fi network you want to connect to
const char* password = "12345678";     // The password of the Wi-Fi network

// for NTP
const char* ntpServer = "asia.pool.ntp.org"; //NTP pool server
const long  gmtOffset_sec = 19800;
const int   daylightOffset_sec = 19800;

char timeWeek[3];
char timeMonth[3];
char timeDay[3];
char timeYear[5];
char timeHour[5];
char timeMinute[6];
char timeSecond[7];


int Week1;
int Month1;
int Day1;
int Year1;
int Hour1;
int Minute1;
int Second1;

String Weekstring;
String Monthstring;
String Daystring;
String Yearstring;
String Hourstring;
String Minutestring;
String Secondstring;

struct tm timeinfo;

//--------------------------------------------
void printLocalTime() {
  //  struct tm timeinfo;
  delay(1000);
  if (!getLocalTime(&timeinfo)) {
    Serial.println("Failed to obtain time");
    return;
  }

  strftime(timeWeek, 3, "%A", &timeinfo);
  Weekstring = String(timeWeek);
  Week1 = Weekstring.toInt();
  //Serial.println(Week1);
  strftime(timeMonth, 3, "%m", &timeinfo);
  String Monthstring = String(timeMonth);
  int Month1 = Monthstring.toInt();
  //Serial.println(Month1);
  strftime(timeDay, 3, "%d", &timeinfo);
  Daystring = String(timeDay);
  Day1 = Daystring.toInt();
  //Serial.println(Day1);
  strftime(timeYear, 5, "%Y", &timeinfo);
  Yearstring = String(timeYear);
  Year1 = Yearstring.toInt();
  //Serial.println(Year1);
  strftime(timeHour, 5, "%H", &timeinfo);
  Hourstring = String(timeHour);
  Hour1 = Hourstring.toInt();
  //Serial.println(Hour1);
  strftime(timeMinute, 6, "%M", &timeinfo);
  Minutestring = String(timeMinute);
  Minute1 = Minutestring.toInt();
  //Serial.println(Minute1);
  strftime(timeSecond, 7, "%S", &timeinfo);
  Secondstring = String(timeSecond);
  Second1 = Secondstring.toInt();

  //   rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));
  rtc.adjust(DateTime(Year1, Month1, Day1, Hour1, Minute1, Second1)); //For adjest RTC time from the internet

  //Serial.println(Second1);
  //  Serial.println(&timeinfo, "%A,%m, %d %Y %H:%M:%S");//for print NTP time



}

//------------------------------------------------------


//==========================================================
// Connect to Wi-Fi
//=========================================================
//********************connect to the WiFi******************
bool myWiFiFirstConnect = true;
void myWiFiTask(void *pvParameters) {
  wl_status_t state;
  while (true) {
    state = WiFi.status();
    if (state != WL_CONNECTED){  // We have no connection
    if (state == WL_NO_SHIELD){  // WiFi.begin wasn't called yet
        Serial.println("Connecting WiFi");
        WiFi.mode(WIFI_STA);
        WiFi.begin(ssid, password);
       }
  else if (state == WL_CONNECT_FAILED) {  // WiFi.begin has failed (AUTH_FAIL)
        Serial.println("Disconnecting WiFi");
        WiFi.disconnect(true);
       } 
  else if (state == WL_DISCONNECTED) {  // WiFi.disconnect was done or Router.WiFi got out of range
    if (!myWiFiFirstConnect) {  // Report only once
        myWiFiFirstConnect = true;
        Serial.println("WiFi disconnected");
        }
        }
      vTaskDelay (250); // Check again in about 250ms
      }
  else { // We have connection
    if (myWiFiFirstConnect) {  // Report only once
        myWiFiFirstConnect = false;
        Serial.print("Connected to ");
        Serial.println(ssid);
        Serial.print("IP address: ");
        Serial.println(WiFi.localIP());
        Serial.println("");
        //printLocalTime();
      }
      vTaskDelay (1000); // Check again in about 5s
    }
  }
}
//=====================================================================

void setup() {
  Serial.begin(115200);
  Wire.begin();
  SPI.begin();      // Init SPI bus
  rtc.begin();
  delay(1000);
// xTaskCreatePinnedToCore(myWiFiTask, "myWiFiTask", 8192, NULL, 3, NULL, 0);  // Create a connection task with 8kB stack on core 0
//      configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
//    printLocalTime();




  mfrc522.PCD_Init();   // Init MFRC522

  delay(1000); // wait for console opening

}

void loop() {


  //check if there's a connection to Wi-Fi or not
//  if (!WiFi.isConnected()) {
//    connectToWiFi();    //Retry to connect to Wi-Fi
//    configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
//    printLocalTime();

  
  //  printLocalTime();
  //____________________________________________________
  //RFID Reading
  if ( ! mfrc522.PICC_IsNewCardPresent()) {
    return;
  }
  // Select one of the cards
  if ( ! mfrc522.PICC_ReadCardSerial()) {
    return;
  }

  //Reading from the card
  String tag = "";
  for (byte i = 0; i < mfrc522.uid.size; i++)
  {
    tag.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " "));
    tag.concat(String(mfrc522.uid.uidByte[i], HEX));
  }
  tag.toUpperCase();
  Serial.print("Card number : ");
  Serial.println(tag.substring(1));
  Serial.print("Time : ");

  DateTime now = rtc.now();
  Serial.print(now.day(), DEC);
  Serial.print("-");
  Serial.print(now.month(), DEC);
  Serial.print("-");
  Serial.print(now.year(), DEC);
  Serial.print("/");
  Serial.print(now.hour(), DEC);
  Serial.print(":");
  Serial.print(now.minute(), DEC);
  Serial.print(":");
  Serial.print(now.second(), DEC);

  Serial.println();
  delay(2000);


  //_______________________________________________________
}
